#!/bin/bash

file="${1}"
jupyter nbconvert --to HTML --execute "${file}" --TagRemovePreprocessor.remove_input_tags "hide-input" --ExecutePreprocessor.timeout=None --no-prompt
