---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["hide-input"]
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import numpy as np
import math
```

```python tags=["hide-input"]
camera = dict(eye=dict(x=1.5, y=1.5, z=1.7))
```

# Tangent Plane: Graphs

Consider the graph of a smooth function $f: D \subseteq \mathbb{R}^2 \to \mathbb{R}$. Just as the graph of $y = f(x)$ in one-dimension has a tangent line at each point, the graph in two-dimensions as a tangent plane at each point. We will give two interpretations of the tangent plane - one as velocity vectors to curves and the other as the image of the differential of the graph parametrisation. The two are related via the chain rule.


# Tangent line

First let us recall the notion of the tangent line for a differentiable function $f: (a, b) \to \mathbb{R}$. Given $x_0 \in (a, b)$, the tangent line $L_{x_0}$ to $f$ at $x_0$ is the line through the point $(x_0, f(x_0))$ and with slope $f'(x_0)$. It has parametric equation
$$
L(u) = (x_0, f(x_0)) + t (1, f'(x_0))
$$

```python
def tangent_line(x0, y0, yp0, color="red", N = 5):
    t = np.linspace(-1, 1, N)
    Lx = x0 + t
    Ly = y0 + yp0 * t
    
    line = go.Scatter(x=Lx, y=Ly, mode="lines", showlegend=False, line=dict(color=color), name="tangent line")
    point = go.Scatter(x=[x0],y=[y0], showlegend=False, mode="markers", marker = dict(size=15, color=color), name="base point")

    return [line, point]
```

```python
def f(x):
    return x**2 + 1

def fp(x):
    return 2*x

fig = go.Figure()

# Plot of function
x = np.linspace(-1, 1, 40)
y = f(x)
fig.add_trace(go.Scatter(x=x, y=y, name="y = f(x)", showlegend=False))

# Tangent line points
N = 30
ix = math.ceil(N/2)
x0 = np.linspace(-1.0, 1.0, N)
y0 = f(x0)
yp0 = fp(x0)

# Create traces
for i in range(len(x0)):
    for t in tangent_line(x0[i], y0[i], yp0[i]):
        t.visible = False
        fig.add_trace(t)

# Make middle trace visible
fig.data[N].visible = True
fig.data[N+1].visible = True

# Create sliders
steps = []
for i in range(N):
    title = fr"$\text{{Tangent line  to }} y = x^2 + 1 \text{{ at }} ({x0[i]:.2f}, {y0[i]:.2f}): L(t) = ({x0[i]:.2f}, {y0[i]:.2f}) + t(1.00, {yp0[i]:.2f})$"
    step = dict(method = "update", args = [{"visible" : [True] + [False]*2*N}, {"title" : title}], label = f"{x0[i]:.2f}")
    step["args"][0]["visible"][2*i + 1] = True
    step["args"][0]["visible"][2*i + 2] = True
    steps.append(step)

sliders = [dict(active=ix, steps=steps, currentvalue={"prefix": "x0: "})]
fig.update_layout(sliders = sliders)

# Figure properties
title = fr"$\text{{Tangent line  to }} y = x^2 + 1 \text{{ at }} ({x0[ix]:.2f}, {y0[ix]:.2f}): L(t) = ({x0[ix]:.2f}, {y0[ix]:.2f}) + t(1.00, {yp0[ix]:.2f})$"
fig.update_layout(title = title)
fig.update_xaxes(range=[-1, 1])
fig.update_yaxes(range=[0.5,2], scaleanchor = "x", scaleratio = 1)

fig.show()
```

# Velocity Vectors

Now let us consider the case of surfaces which are two dimension, hence we expect to have a tangent plane, which we can also think of as two dimensions of tangent lines. There are two distinguished such lines, the **coordinate lines**. Given a point $(u_0, v_0)$ in the parameter space we have the lines
$$
\tilde{L}_u(t) = (u_0 + t, v_0), \quad \tilde{L}_v(t) = (u_0, v_0 + t)
$$
parallel to the $u$ and $v$ axes respectively. The corresponding lines on the surface are
$$
L_u(t) = \Phi \circ \tilde{L}_u(t) = \big(u_0 + t, v_0, f(u_0 + t, v_0)\big), \quad L_v(t) = \Phi \circ \tilde{L}_v(t) = \big(u_0, v_0 + t, f(u_0, v_0 + t)\big)
$$
where $\Phi(u, v) = (u, v, f(u, v)$ is the surface parametrisation.

```python tags=[]
u = np.linspace(-1, 1,20)
v = np.linspace(-1, 1, 20)
U, V = np.meshgrid(u, v)

def f(x, y):
    return x**2 - y**2

Z = f(U, V)

t  = np.linspace(-1, 1, 10)
u_0, v_0 = 0, 0
Lu = pd.DataFrame(data = dict(x=u_0 + t, y=v_0, z=f(u_0 + t, v_0), coord="u"))
Lv = pd.DataFrame(data = dict(x=u_0, y=v_0 + t, z=f(u_0, v_0+t), coord="v"))
fig = px.line_3d(pd.concat([Lu, Lv]), x="x", y="y", z="z", color="coord")
fig.add_trace(go.Surface(x=U, y=V, z=Z))

fig.update_layout(title = r"$z = x^2 - y^2$", scene_camera = camera)
fig.show()
```

The velocity vectors to the coordinate tangent lines are known as the **coordinate tangent vectors**. That is,
$$
\vec{e}_u (u_0, v_0) = \partial_t|_{t=0} L_u(t) = \partial_t|_{t=0} \big(u_0 + t, v_0, f(u_0 + t, v_0)\big) = \big(1, 0, \partial_u f (u_0, v_0)\big).
$$
Likewise,
$$
\vec{e}_v (u_0, v_0) = \partial_t|_{t=0} L_v(t) = \partial_t|_{t=0} \big(u_0, v_0 + t, f(u_0, v_0 + t)\big) = \big(0, 1, \partial_v f (u_0, v_0)\big).
$$

The plane through the point $p_0 = \Phi(u_0, v_0) = (u_0, v_0, f(u_0, v_0))$ and spanned by $\vec{e}_u(u_0, v_0), \vec{e}_v(u_0, v_0)$ is the **tangent plane** to the surface at the point $p_0$.
